FROM php:latest


RUN apt-get update -y
RUN apt-get install -y git libcurl4-gnutls-dev libicu-dev libmcrypt-dev libvpx-dev libjpeg-dev sqlite3 libpng-dev libxpm-dev zlib1g-dev libfreetype6-dev libxml2-dev libexpat1-dev libbz2-dev libgmp3-dev libldap2-dev unixodbc-dev libpq-dev libsqlite3-dev libaspell-dev libsnmp-dev libpcre3-dev libtidy-dev -yqq
RUN docker-php-ext-install mbstring pdo pdo_mysql pdo_sqlite curl json intl gd xml zip bz2 opcache

